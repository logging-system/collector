package helpers

import (
	"log"
)

func CheckInternalFatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
