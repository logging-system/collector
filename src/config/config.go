package config

import "flag"

type AppConfig struct {
	EntryPoint string
	AccessKey  string
}

var appConfig *AppConfig

func GetConfig() AppConfig {
	if appConfig != nil {
		return *appConfig
	}

	var config AppConfig

	flag.StringVar(&config.EntryPoint, "ep", "", "[required] Entry point to your log system Example -ep http://my.log.system.com")
	flag.StringVar(&config.AccessKey, "key", "", "[required] Access key to your log system. Example -key myaccesskey")

	flag.Parse()

	appConfig = &config

	return *appConfig
}
