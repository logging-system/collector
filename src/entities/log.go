package entities

import "time"

const (
	LOG_LEVEL_TRACE = 1
	LOG_LEVEL_DEBUG = 2
	LOG_LEVEL_INFO  = 3
	LOG_LEVEL_WARN  = 4
	LOG_LEVEL_ERROR = 5
	LOG_LEVEL_FATAL = 6
)

type LogEntity struct {
	Id        string                 `json:"id"`
	Level     int                    `json:"level"`
	Message   string                 `json:"message"`
	Service   string                 `json:"service"`
	Timestamp time.Time              `json:"timestamp"`
	Data      map[string]interface{} `json:"data"`
}
