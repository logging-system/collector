package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strings"

	"gitlab.com/logging-system/collector/config"
	"gitlab.com/logging-system/collector/entities"
	"gitlab.com/logging-system/collector/helpers"
)

func main() {
	scanner := bufio.NewScanner(os.Stdin)

	for scanner.Scan() {
		lines := strings.Split(scanner.Text(), "\n")

		for _, line := range lines {
			var log entities.LogEntity

			err := json.Unmarshal([]byte(line), &log)

			if err != nil {
				fmt.Println("Log format is invalid", line)
			}

			sendToServer(line)
		}
	}

	err := scanner.Err()
	helpers.CheckInternalFatal(err)
}

func sendToServer(logLine string) {
	appConfig := config.GetConfig()
	url := fmt.Sprint(appConfig.EntryPoint, "/api/log/create")
	req, err := http.NewRequest("POST", url, bytes.NewBuffer([]byte(logLine)))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", appConfig.AccessKey)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println("Status code:", resp.StatusCode)

	defer resp.Body.Close()
}
