FROM golang:1.20.2-buster

USER root
RUN mkdir -p /opt/app

COPY ./ /opt/app

RUN cd /opt/app/src && GOOS=linux GOARCH=amd64 go build -o ../build/log-collector .