Dastanaron Log System Collector
==========================

Log collector from your docker containers. It is suitable if your logs are in the format of this system or you are using libraries provided by this system. The format of the logs suitable for collection is shown below:

```json
{
  "id": "86084a86-2b92-4a7c-a072-29087abe8389",
  "level": 3,
  "message": "Access denied",
  "service": "dastanaron-log-system",
  "timestamp": "2023-04-16T13:04:23.041900689Z",
  "data": {
    "accessKey": "",
    "ip": "127.0.0.1:39806"
  }
}
```
!Important. The ID must be unique; duplicates in the database are cut off by it.

## Example
You need to find out the name of your docker container and run the command once every one minute or more

```bash
docker logs mycontainer | /path-to-app/log-collector -ep http://yourhost.com -key yourkey
```

You can use task schedulers like cron or others to call this function.